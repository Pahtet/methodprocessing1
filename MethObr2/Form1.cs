﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MethObr2
{
    public partial class Form1 : Form
    {
        Bitmap originalImage;
        private Graphics graphics;

        public Form1()
        {
            InitializeComponent();
            graphics = this.CreateGraphics();
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (open.ShowDialog() == DialogResult.OK)
            {
                originalImage = new Bitmap(open.FileName);
                drawImage();
            }
        }

        private void drawImage()
        {
            Point p = new Point(32, 100);
            graphics.DrawImage(originalImage, p);
        }
    }
}
