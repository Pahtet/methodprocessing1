﻿namespace MethObr1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOpenImage = new System.Windows.Forms.Button();
            this.buttonInvert = new System.Windows.Forms.Button();
            this.buttonAddBrightness = new System.Windows.Forms.Button();
            this.buttonBinarize = new System.Windows.Forms.Button();
            this.trackBarValue = new System.Windows.Forms.TrackBar();
            this.buttonChangeContrast = new System.Windows.Forms.Button();
            this.labelSliderValue = new System.Windows.Forms.Label();
            this.buttonCutColor = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarValue)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonOpenImage
            // 
            this.buttonOpenImage.Location = new System.Drawing.Point(13, 13);
            this.buttonOpenImage.Name = "buttonOpenImage";
            this.buttonOpenImage.Size = new System.Drawing.Size(75, 23);
            this.buttonOpenImage.TabIndex = 0;
            this.buttonOpenImage.Text = "Open";
            this.buttonOpenImage.UseVisualStyleBackColor = true;
            this.buttonOpenImage.Click += new System.EventHandler(this.buttonOpenImage_Click);
            // 
            // buttonInvert
            // 
            this.buttonInvert.Location = new System.Drawing.Point(95, 13);
            this.buttonInvert.Name = "buttonInvert";
            this.buttonInvert.Size = new System.Drawing.Size(75, 23);
            this.buttonInvert.TabIndex = 1;
            this.buttonInvert.Text = "Invert";
            this.buttonInvert.UseVisualStyleBackColor = true;
            this.buttonInvert.Click += new System.EventHandler(this.buttonInvert_Click);
            // 
            // buttonAddBrightness
            // 
            this.buttonAddBrightness.Location = new System.Drawing.Point(177, 12);
            this.buttonAddBrightness.Name = "buttonAddBrightness";
            this.buttonAddBrightness.Size = new System.Drawing.Size(95, 23);
            this.buttonAddBrightness.TabIndex = 2;
            this.buttonAddBrightness.Text = "add Brightness";
            this.buttonAddBrightness.UseVisualStyleBackColor = true;
            this.buttonAddBrightness.Click += new System.EventHandler(this.buttonAddBrightness_Click);
            // 
            // buttonBinarize
            // 
            this.buttonBinarize.Location = new System.Drawing.Point(278, 13);
            this.buttonBinarize.Name = "buttonBinarize";
            this.buttonBinarize.Size = new System.Drawing.Size(75, 23);
            this.buttonBinarize.TabIndex = 3;
            this.buttonBinarize.Text = "binarize";
            this.buttonBinarize.UseVisualStyleBackColor = true;
            this.buttonBinarize.Click += new System.EventHandler(this.buttonBinarize_Click);
            // 
            // trackBarValue
            // 
            this.trackBarValue.LargeChange = 10;
            this.trackBarValue.Location = new System.Drawing.Point(13, 43);
            this.trackBarValue.Maximum = 100;
            this.trackBarValue.Minimum = -100;
            this.trackBarValue.Name = "trackBarValue";
            this.trackBarValue.Size = new System.Drawing.Size(422, 45);
            this.trackBarValue.SmallChange = 5;
            this.trackBarValue.TabIndex = 4;
            this.trackBarValue.ValueChanged += new System.EventHandler(this.trackBarValue_ValueChanged);
            // 
            // buttonChangeContrast
            // 
            this.buttonChangeContrast.Location = new System.Drawing.Point(360, 12);
            this.buttonChangeContrast.Name = "buttonChangeContrast";
            this.buttonChangeContrast.Size = new System.Drawing.Size(75, 23);
            this.buttonChangeContrast.TabIndex = 5;
            this.buttonChangeContrast.Text = "changeContrast";
            this.buttonChangeContrast.UseVisualStyleBackColor = true;
            this.buttonChangeContrast.Click += new System.EventHandler(this.buttonChangeContrast_Click);
            // 
            // labelSliderValue
            // 
            this.labelSliderValue.AutoSize = true;
            this.labelSliderValue.Location = new System.Drawing.Point(441, 57);
            this.labelSliderValue.Name = "labelSliderValue";
            this.labelSliderValue.Size = new System.Drawing.Size(13, 13);
            this.labelSliderValue.TabIndex = 6;
            this.labelSliderValue.Text = "0";
            // 
            // buttonCutColor
            // 
            this.buttonCutColor.Location = new System.Drawing.Point(444, 12);
            this.buttonCutColor.Name = "buttonCutColor";
            this.buttonCutColor.Size = new System.Drawing.Size(75, 23);
            this.buttonCutColor.TabIndex = 7;
            this.buttonCutColor.Text = "cutColor";
            this.buttonCutColor.UseVisualStyleBackColor = true;
            this.buttonCutColor.Click += new System.EventHandler(this.buttonCutColor_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 297);
            this.Controls.Add(this.buttonCutColor);
            this.Controls.Add(this.labelSliderValue);
            this.Controls.Add(this.buttonChangeContrast);
            this.Controls.Add(this.trackBarValue);
            this.Controls.Add(this.buttonBinarize);
            this.Controls.Add(this.buttonAddBrightness);
            this.Controls.Add(this.buttonInvert);
            this.Controls.Add(this.buttonOpenImage);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trackBarValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOpenImage;
        private System.Windows.Forms.Button buttonInvert;
        private System.Windows.Forms.Button buttonAddBrightness;
        private System.Windows.Forms.Button buttonBinarize;
        private System.Windows.Forms.TrackBar trackBarValue;
        private System.Windows.Forms.Button buttonChangeContrast;
        private System.Windows.Forms.Label labelSliderValue;
        private System.Windows.Forms.Button buttonCutColor;
    }
}

