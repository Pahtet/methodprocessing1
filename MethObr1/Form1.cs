﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MethObr1
{
    public partial class Form1 : Form
    {
        private Bitmap originalImage;
        private Bitmap modImage;
        private Graphics graphics;

        public Form1()
        {
            InitializeComponent();
            graphics = this.CreateGraphics();
        }

        private void buttonOpenImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (open.ShowDialog() == DialogResult.OK)
            {
                originalImage = new Bitmap(open.FileName);
                drawImage();
            }
        }

        private void drawImage()
        {

            Point p = new Point(32, 100);
            graphics.DrawImage(getCurrentImage(), p);           
        }

        private Bitmap getCurrentImage()
        {
            if(null != modImage)
            {
                return modImage;
            }
            return originalImage;
        }

        private void workWithPixels(BitmapMethods.pixelDelegate func)
        {
            Size s = originalImage.Size;
            modImage = new Bitmap(getCurrentImage());
            for(int x = 0; x<s.Width; x++)
            {
                for(int y = 0; y<s.Height; y++)
                {
                    func(modImage,x, y);
                }
            }
        }

        private void buttonInvert_Click(object sender, EventArgs e)
        {
            workWithPixels(BitmapMethods.invertPixelDelegate);
            drawImage();
        }

        private void buttonAddBrightness_Click(object sender, EventArgs e)
        {
            workWithPixels(BitmapMethods.addBrightnessPixelDelegate);
            drawImage();
        }

        private void buttonBinarize_Click(object sender, EventArgs e)
        {
            workWithPixels(BitmapMethods.binarizeImageDelegate);
            drawImage();
        }

        private void buttonChangeContrast_Click(object sender, EventArgs e)
        {
            double value = trackBarValue.Value;
            BitmapMethods.changeContrast(getCurrentImage(), value);
            drawImage();

        }

        private void trackBarValue_ValueChanged(object sender, EventArgs e)
        {
            labelSliderValue.Text = trackBarValue.Value.ToString();
        }

        private void buttonCutColor_Click(object sender, EventArgs e)
        {
            int value = trackBarValue.Value * 255;
            BitmapMethods.colorCut(getCurrentImage(), value);
            drawImage();
        }

    }
}
