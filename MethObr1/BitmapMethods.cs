﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethObr1
{
    class BitmapMethods
    {
        public delegate void pixelDelegate(Bitmap bitmap, int x, int y);

        public static void invertPixelDelegate(Bitmap bitmap, int x, int y)
        {
            Color c = bitmap.GetPixel(x, y);
            Color newColor = Color.FromArgb(c.A, 255 - c.R, 255 - c.G, 255 - c.B);
            bitmap.SetPixel(x, y, newColor);
        }

        public static void addBrightnessPixelDelegate(Bitmap bitmap, int x, int y)
        {
            Color c = bitmap.GetPixel(x, y);
            Color newColor = Color.FromArgb(
                (int)c.A,
                (int)Math.Min(c.R * 1.2, 255),
                (int)Math.Min(c.G * 1.2, 255),
                (int)Math.Min(c.B * 1.2, 255));
            bitmap.SetPixel(x, y, newColor);
        }

        public static void binarizeImageDelegate(Bitmap bitmap, int x, int y)
        {
            Color c = bitmap.GetPixel(x, y);
            int porog = 200;
            Color newColor = Color.FromArgb(
                (int)c.A,
                c.R > porog ? 255 : 0,
                c.G > porog ? 255 : 0,
                c.B > porog ? 255 : 0);
            bitmap.SetPixel(x, y, newColor);
        }

        public static void changeContrast(Bitmap bitmap, double value)
        {
            Color cur_pixel;
            ulong R_avg = 0, G_avg = 0, B_avg = 0;
            for (int i = 0; i < bitmap.Width; ++i)
            {
                for (int j = 0; j < bitmap.Height; ++j)
                {
                    cur_pixel = bitmap.GetPixel(i, j);
                    R_avg += cur_pixel.R;
                    G_avg += cur_pixel.G;
                    B_avg += cur_pixel.B;
                }
            }
            ulong numberOfPixels = (uint)bitmap.Width * (uint)bitmap.Height;
            R_avg /= numberOfPixels;
            G_avg /= numberOfPixels;
            B_avg /= numberOfPixels;
            for (int i = 0; i < bitmap.Width; ++i)
            {
                for (int j = 0; j < bitmap.Height; ++j)
                {
                    cur_pixel = bitmap.GetPixel(i, j);
                    bitmap.SetPixel(i, j, Color.FromArgb(
                        colorNormalization(value * (cur_pixel.R - R_avg) + R_avg),
                        colorNormalization(value * (cur_pixel.G - G_avg) + G_avg),
                        colorNormalization(value * (cur_pixel.B - B_avg) + B_avg)
                        ));
                }
            }

        }

        public static void colorCut(Bitmap bitmap, int limit)
        {
            Func<Color,int,Color> action;
            Color cur_pixel;
            if(0 > limit)
            {
                action = cutColorNegative;
            }
            else
            {
                action = cutColorPositive;
            }
            for (int i = 0; i < bitmap.Width; ++i)
            {
                for (int j = 0; j < bitmap.Height; ++j)
                {
                    cur_pixel = bitmap.GetPixel(i, j);
                    bitmap.SetPixel(i, j, action(cur_pixel, limit));
                }
            }                
        }

        private static Color cutColorNegative(Color current, int limit)
        {
            return Color.FromArgb(
                (current.R <= limit * -1) ? (0) : (current.R),
                (current.G <= limit * -1) ? (0) : (current.G),
                (current.B <= limit * -1) ? (0) : (current.B));
        }

        private static Color cutColorPositive(Color current, int limit)
        {
            return Color.FromArgb(
                (current.R >= limit) ? (255) : (current.R),
                (current.G >= limit) ? (255) : (current.G),
                (current.B >= limit) ? (255) : (current.B));
        }

        private static int colorNormalization(double color)
        {
            if (0.0 >= color)
                return 0;
            if (255.0 <= color)
                return 255;
            return (int)color;
        }
    }
}
